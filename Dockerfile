FROM python:3.8-alpine

WORKDIR /app

COPY . .
RUN pip install pipenv && pipenv install --system --deploy && rm Pipfile* && chmod +x cmd.sh


CMD ["./cmd.sh"]