from django.apps import AppConfig


class HatmanConfig(AppConfig):
    name = 'hatman'
