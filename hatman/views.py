from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from hatman.forms import SignUpForm, ProfileForm

from hatman.models import Date, Response

from datetime import date, timedelta


def index(request):
	if request.user.is_authenticated:
		if request.method == 'POST':
			did = request.POST['date_id']
			uid = request.user.profile.id
			response = request.POST['response'] == "True"
			Response.objects.update_or_create(date_id=did, person_id=uid, defaults={'response': response})
			return redirect('index')
	
		date_range = [date.today(), date.today() + timedelta(days=7)]
		next_date = Date.objects.filter(date__range=date_range)
	
		dates = [
			{'date': d, 'response': d.response_set.filter(person__user=request.user).first()} for d in next_date
		]

		context = {'next_date': dates, }
		return render(request, 'upcoming.html', context)
	
	else:
		return redirect('login')


def signup(request):
	if request.method == 'POST':
		user_form = SignUpForm(request.POST)
		profile_form = ProfileForm(request.POST)
		if user_form.is_valid() and profile_form.is_valid():
			user = user_form.save()
			user.refresh_from_db()
			user.profile.phone = profile_form.cleaned_data.get('phone')
			user.profile.display_name = profile_form.cleaned_data.get('display_name')
			user.profile.playing_gender = profile_form.cleaned_data.get('playing_gender')
			user.save()
			raw_password = user_form.cleaned_data.get('password1')
			user = authenticate(username=user.username, password=raw_password)
			login(request, user)
			return redirect('/')
	else:
		user_form = SignUpForm()
		profile_form = ProfileForm()
	return render(request, 'signup.html', {'user_form': user_form, 'profile_form': profile_form})
