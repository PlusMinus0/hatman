from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from hatman.models import Profile


class SignUpForm(UserCreationForm):
	"""Form for registration"""

	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'password1', 'password2', )
		

class ProfileForm(forms.ModelForm):
	phone = forms.RegexField(regex=r"(\+\d{2,3}[ -]?|0)[ \d]+", required=False,
	                         help_text='Optional, für den Corona-Bogen (vielleicht)')

	class Meta:
		model = Profile
		fields = ('phone', 'display_name', 'playing_gender')

