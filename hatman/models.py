from django.db import models
from django.contrib.auth.models import User
from django.db.models import UniqueConstraint
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
import uuid


class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	phone = models.CharField(max_length=20, blank=True)
	display_name = models.CharField(max_length=20, unique=True)

	class PlayingGender(models.TextChoices):
		FEMALE = 'F', _('Female')
		MALE = 'M', _('Male')

	playing_gender = models.CharField(
		max_length=2,
		choices=PlayingGender.choices,
	)
	
	def __str__(self):
		return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
	if created:
		Profile.objects.create(user=instance, display_name=uuid.uuid1())


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
	instance.profile.save()


class Date(models.Model):
	date = models.DateField()
	attendees = models.ManyToManyField(Profile, through='Response')
	
	def __str__(self):
		return str(self.date)


class Response(models.Model):
	person = models.ForeignKey(Profile, on_delete=models.CASCADE)
	date = models.ForeignKey(Date, on_delete=models.CASCADE)
	
	response = models.BooleanField()
	
	class Meta:
		constraints = [
			UniqueConstraint(fields=['person', 'date'], name='unique_response'),
		]



